## Consumer configuration

### Generate Keys

In order to be able to use a COIN API, the user is required to have a private/public key pair. Execute the following commands:
```
ssh-keygen -m PEM -t rsa -b 4096 -f private-key.pem -N '' 
ssh-keygen -e -m PKCS8 -f private-key.pem > public-key.pem
```

For Windows users, please note that running the commands in `git-bash` is required:
```
ssh-keygen.exe -m PEM -t rsa -b 4096 -f private-key.pem -N ''
ssh-keygen.exe -e -m PKCS8 -f private-key.pem > public-key.pem
```

These scripts generate the private and public keys: `public-key.pem` and `private-key.pem`.

### Configure your consumer in COIN's IAM (Identity and Access Management)

- Go to: https://test-portal.coin.nl/iam
    - Access to this site requires a user account that can be requested at the [COIN Servicedesk](mailto:servicedesk@coin.nl).
    - They will also create a consumer for you with access to the API you want to use.

- Optional: select language
  ![select language](./img/coin_iam_select_language.png "Select Consumer")

- Go to 'My consumers' and click 'Edit'
  ![select consumer](./img/coin_iam_select_consumer.png "Select Consumer")

- Configure IP Addresses, add public key and press 'Save'
  ![edit consumer](./img/coin_iam_edit_consumer.png "Configure IPs and public key")

- Copy the encrypted hmac secret and paste it into a file named `sharedkey.encrypted`
  ![copy encrypted hmac secret](./img/coin_iam_copy_hmac.png "Retrieve Client Credentials")

## Using a COIN API with your consumer
Now that your consumer has been configured, you can use it to connect to a COIN API.
You can try this out on our [Swagger-UI](https://test-api.coin.nl/docs) page.
To easily develop software that connects to the API, SDKs in various programming languages are provided on our [public gitlab page](https://gitlab.com/verenigingcoin-public).
Please check out the README in the project of your preferred programming language.

## Additional Resources
Find everything you need at the [COIN portal](https://test-portal.coin.nl/).

## Support
If you need support, feel free to send an email to the [COIN servicedesk](mailto:servicedesk@coin.nl).
